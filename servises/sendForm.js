export let category = "";
export const getCategory = () => {
  return category;
};

export const setCategory = (data) => {
  category = data;
};

export const sendForm = (data) => {
  if (!category || !data.name || !data.price || !data.image) {
    alert("Заполните все поля");
    return;
  }
  data.category = category;
  console.log(data);
  return true;
};
