import { CategoriesHeader } from "../components/categoriesHeader/categoriesHeader";
import { CategoryAside } from "../components/aside/CategoryAside";
import { ProductCard } from "../components/cards/productCard";

const Categories = () => {
  const textPropImg = [
    {
      name: "Pizza",
      price: "1000",
      img: "/pizza.jpg",
    },
    {
      name: "Pizza",
      price: "1000",
      img: "/pizza.jpg",
    },
    {
      name: "Pizza",
      price: "1000",
      img: "/pizza.jpg",
    },
    {
      name: "Pizza",
      price: "1000",
      img: "/pizza.jpg",
    },
    {
      name: "Pizza",
      price: "1000",
      img: "/pizza.jpg",
    },
    {
      name: "Pizza",
      price: "1000",
      img: "/pizza.jpg",
    },
    {
      name: "Pizza",
      price: "1000",
      img: "/pizza.jpg",
    },
    {
      name: "Pizza",
      price: "1000",
      img: "/pizza.jpg",
    },
    {
      name: "Pizza",
      price: "1000",
      img: "/pizza.jpg",
    },
    {
      name: "Pizza",
      price: "1000",
      img: "/pizza.jpg",
    },
    {
      name: "Pizza",
      price: "1000",
      img: "/pizza.jpg",
    },
    {
      name: "Pizza",
      price: "1000",
      img: "/pizza.jpg",
    },
  ];
  return (
    <div className="w-full flex justify-center ">
      <div className="work-palce border-s rounded-md flex p-10  border border-indigo-400">
        <CategoryAside />
        <ProductCard cards={textPropImg} />
      </div>
    </div>
  );
};
export default Categories;
