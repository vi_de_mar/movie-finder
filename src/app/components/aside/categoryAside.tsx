"use client";
import React, { useState } from "react";
import Link from "next/link";
import categories from "../../../../static/categories";
import { getCategory, setCategory } from "../../../../servises/sendForm";

const CategoryAside = () => {
  const [name, setName] = useState();
  const className =
    "cursor-pointer m-2 border-2  rounded-md shadow-xl border-indigo-200 hover:border-indigo-600";
  const checkTypeProducts = (link) => {
    setCategory(link);
    setName(link);
  };

  return (
    <ul className="menu flex flex-col  pb-3  ">
      {categories.menu.map((link) => (
        <li
          className={
            name === link.title ? className + " border-indigo-600" : className
          }
          key={link.id}
          onClick={() => checkTypeProducts(link.title)}
        >
          <div className="px-4"> {link.title}</div>
        </li>
      ))}
    </ul>
  );
};

export { CategoryAside };
