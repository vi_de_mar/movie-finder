import Image from "next/image";
import profileMenu from "../../../../static/profileMenu";

const ProfileAside = () => {
  const className =
    "cursor-pointer m-2 border-2  rounded-md shadow-xl border-indigo-200 hover:border-indigo-600";
  return (
    <div className="profile-aside">
      <div className="profile-header flex flex-col items-center ">
        <Image
          className="rounded-full	"
          src="/icon.png"
          width="80"
          height="80"
          alt="profile"
        />
        <div className="user-name">Administrator</div>
      </div>
      <ul className="menu flex flex-col  pb-3 mt-10  ">
        {profileMenu.menu.map((item) => (
          <li
            className={className}
            key={item.id}
            // onClick={() => checkTypeProducts(item.title)}
          >
            <div className="px-4"> {item.title}</div>
          </li>
        ))}
      </ul>
    </div>
  );
};
export { ProfileAside };
