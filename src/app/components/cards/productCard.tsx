import Image from "next/image";

const ProductCard = (props) => {
  const cards = props.cards;
  return (
    <>
      <div className="flex flex-wrap ">
        {cards.map((card) => (
          <div
            key={card.name}
            className="productCard-wrapper cursor-pointer px-5  ml-20 m-4  border-2  rounded-md shadow-xl border-indigo-200 hover:border-indigo-600"
          >
            <Image src={card.img} width="100" height="100" alt="pizza" />
            <div className="productCard-description flex justify-between mt-5">
              <div className="productCard-name">{card.name}</div>
              <div className="productCard-price">{card.price} $</div>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};
export { ProductCard };
