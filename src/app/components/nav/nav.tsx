import Link from "next/link";
import navData from "../../../../static/nav";

const Nav = () => {
  return (
    <ul className="nav flex justify-center flex-wrap pb-3 ">
      {navData.nav.map((link) => (
        <li
          className=" mx-1 my-1 md:mx-2 border-2 rounded-md shadow-xl border-indigo-200 hover:border-indigo-600"
          key={link.id}
        >
          <Link className="px-4" href={link.href}>
            {link.title}{" "}
          </Link>
        </li>
      ))}
    </ul>
  );
};

export { Nav };
