"use client";
import React, { useState } from "react";
import { sendForm } from "../../../../../servises/sendForm";
const AddMenuForm = () => {
  const inputClasses = `w-full px-3 py-2 bg-white border  rounded-md text-sm shadow-sm 
  focus:outline-none focus:border-sky-500 focus:ring-sky-500 max-w-64 m-1`;
  const formClasses = `flex flwx-wrap max-w-lg`;

  const [name, setName] = useState();
  const [price, setPrice] = useState();
  const [image, setImage] = useState();

  const addnewProduct = (e) => {
    e.preventDefault();
    const data = {
      name,
      price,
      image,
    };
    if (sendForm(data)) {
      setName("");
      setPrice("");
      setImage("");
    }
  };

  return (
    <div className="form  flex justify-center w-full border-l border-indigo-400 p-2">
      <form
        onSubmit={addnewProduct}
        className="flex flex-wrap justify-center  "
      >
        <input
          className={inputClasses}
          value={name}
          type="text"
          placeholder="Input product name"
          onChange={(e) => setName(e.target.value)}
        />

        <input
          className={inputClasses}
          value={price}
          type="text"
          placeholder="Input product price"
          onChange={(e) => setPrice(e.target.value)}
        />
        <input
          className={`${inputClasses} max-w-96 `}
          value={image}
          type="text"
          placeholder="Input product image"
          onChange={(e) => setImage(e.target.value)}
        />
        <button
          type="submit"
          className="w-9/12 p-2 mt-1 border-2 rounded-md shadow-xl border-indigo-200 hover:border-indigo-600"
        >
          Save
        </button>
      </form>
    </div>
  );
};

export default AddMenuForm;
