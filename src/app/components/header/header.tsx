import { Nav } from "@/app/components/nav/nav";
const Header = () => {
  return (
    <>
      <div className="header flex justify-center p-8 ">
        <span className="text-sm sm:text-xl font-semibold	">
          Product CMS for Our Company
        </span>
      </div>
      <div className="navigation">
        <Nav />
      </div>
    </>
  );
};

export { Header };
