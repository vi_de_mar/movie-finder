import { ProfileAside } from "@/app/components/aside/profileAside";
import { ProfileDescription } from "@/app/components/profile/profileDescription";
const Profile = () => {
  return (
    <div className="flex justify-center w-full">
      <div className="profile-wrapper flex">
        <ProfileAside />
        <ProfileDescription />
      </div>
    </div>
  );
};
export default Profile;
