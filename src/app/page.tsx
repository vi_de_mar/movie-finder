import AddMennuFrom from "@/app/components/forms/addMenuForm/addMenuForm";
import { Aside } from "./components/aside/aside";
import React, { useState } from "react";
import { getCategory } from "../../servises/sendForm";

const Home = () => {
  getCategory();
  return (
    <div className="w-full flex justify-center flex-col items-center ">
      <h1 className="text-sm sm:text-xl font-semibold mb-5  "> Add product</h1>
      <div>
        <div className="work-palce border-s rounded-md flex p-10  border border-indigo-400">
          <Aside />
          <AddMennuFrom />
        </div>
      </div>
    </div>
  );
};
export default Home;
