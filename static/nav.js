const navData = {
  nav: [
    { id: 1, title: "Home", href: "/" },
    { id: 2, title: "Categories", href: "/categories" },
    { id: 3, title: "Profile", href: "/profile" },
  ],
};
export default navData;
